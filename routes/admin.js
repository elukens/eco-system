var express = require('express');
var router = express.Router();

var Users = require('../models/modelUser');
var Games = require('../models/game');

router.get('/', isLoggedIn, function(req, res, next) {
  res.render('admin.html', { user: req.user });
});
  
router.get('/players', isLoggedIn, function(req, res, next) {

    Users.find({}, function(err, users) {
        res.render('admin-players.html', { user: req.user, users: users });
    });
    
});

router.get('/games', isLoggedIn, function(req, res, next) {

    Games.find({})
    .populate('owner')
    .exec(function(err, games) {
        res.render('admin-games.html', { user: req.user, games: games });
    });
    
});

router.post('/games/new', isLoggedIn, function(req, res, next) {

    game = new Games(req.body);
    game.owner = req.user._id;
    game.in_progress = 0;
    game.save(function(err, result) {
        if (err) console.log(err);
        res.redirect('/admin/games');
    });

})

router.get('/games/new', isLoggedIn, function(req, res, next) {
    res.render('admin-games-new.html', { user: req.user });
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.admin == 1) {
            return next();
        }
        res.redirect('/account');
    }
    res.redirect('/login');
}


module.exports = router;