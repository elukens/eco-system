var express = require('express');
var router = express.Router();

var Messages = require('../models/messaging');
var Users = require('../models/modelUser');

router.get('/', isLoggedIn, function(req, res, next) {

    Messages.find({toUser: req.user._id})
        .populate('fromUser') 
        .populate('toUser') 
        .exec(function(err, messages) {
            
        res.render('messaging.html', { user: req.user, messages: messages });      
    });
  
});
  
router.get('/new', isLoggedIn, function(req, res, next) {

    Users.find({}, function (err, users) {
        res.render('messaging-new.html', { user: req.user, users: users });      
    });
});

router.post('/new', isLoggedIn, function(req, res, next) {

    message = new Messages(req.body);
    message.read_status = 0;
    message.save(function (err, result) {
        if (err) console.log(err);
        res.redirect('/messaging');
    })

});

router.get('/:Id', isLoggedIn, function(req, res, next) {

    Messages.findById(req.params.Id)
    .populate('fromUser')
    .populate('toUser')
    .exec(function(err, message) {
        console.log(message);
        message.read_status = 1;
        message.save();
        res.render('messaging-read.html', {user: req.user, message: message})
    });

});

router.get('/reply/:Id', isLoggedIn, function(req, res, next) {

    Messages.findById(req.params.Id)
    .populate('fromUser')
    .populate('toUser')
    .exec(function(err, message) {
        console.log(message);
        res.render('messaging-reply.html', {user: req.user, message: message})
    });    

});

router.post('/reply/:Id', isLoggedIn, function(req, res, next) {

    Messages.findById(req.params.Id)
    .exec(function(err, message) {
        message.reply_status = 1;
        message.save(function(err, result) {
            message = new Messages(req.body);
            message.read_status = 0;
            message.save(function (err, result) {
                if (err) console.log(err);
                res.redirect('/messaging');
            })            
        });        
    })


});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;