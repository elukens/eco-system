var express = require('express');
var router = express.Router();

var Games = require('../models/game');
var Businesses = require('../models/business');
var Helpers = require('../helpers');

router.get('/', isLoggedIn, function(req, res, next) {

    // find games that have empty slots and are not running
    Games.find({'in_progress': 0})
    .populate('owner')
    .and({
        $or: [  
            { 'manufacturer': { $exists: false }},
            { 'distributor': { $exists: false }},
            { 'wholesaler': { $exists: false }},
            { 'retailer': { $exists: false }}
        ]
    })
    .exec(function(err, games) {

        // find games that are running that we are a part of
        Games.find({'in_progress': 1})
        .populate('manufacturer')
        .populate('distributor')
        .populate('wholesaler')
        .populate('retailer')        
        .exec(function(err, running_games) {
            // we can probably write a mongoose query to handle this
            runningGames = [];
            for (i = 0; i <= running_games.length - 1; i++) {
                if (Helpers.playerInGame(running_games[i], req.user)) {
                    runningGames.push(running_games[i]);
                }
            }
            res.render('game.html', { user: req.user, games: games, running_games: runningGames });
        });
    })
    
});

router.get('/join/:Id', isLoggedIn, function(req, res, next) {

    Games.findById(req.params.Id)
    .populate('manufacturer')
    .populate('distributor')
    .populate('wholesaler')
    .populate('retailer')
    .exec(function(err, game) {
        if (Helpers.playerInGame(game, req.user)) {
            res.render('game-turn.html', { user: req.user, game: game});
        } else {
            res.render('game-join.html', { user: req.user, game: game});
        }
    });

});

router.get('/select/:Id/:Type', isLoggedIn, function(req, res, next) {

    Games.findById(req.params.Id, function(err, game) {

        // slightly janky, clean this up
        var Business = new Businesses();
        Business.stock = game.starting_stock;
        Business.backorders = game.backorder_stock;
        Business.orders = game.incoming_orders;
        Business.players = req.user;

        Business.save(function(err) {
            if (req.params.Type == 1) {
                game.manufacturer = Business._id;
            } else if (req.params.Type == 2) {
                game.distributor = Business._id;
            } else if (req.params.Type == 3) {
                game.wholesaler = Business._id;
            } else if (req.params.Type == 4) {
                game.retailer = Business._id;
            }

            game.save(function(err) {
                res.render('game-join.html', { user: req.user, game: game});
            })
            
        })
        
    });

});

  
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;