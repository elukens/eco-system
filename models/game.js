var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var gameScehma = mongoose.Schema({
    
    gametype: Number, // 1 = short game, 2 = long game
    
    name: String,
    owner: {
        type: Schema.ObjectId,
        ref: 'User'
    },

    cost_per_item: Number,
    cost_per_backstock: Number,

    backorder_stock: Number,
    starting_stock: Number,
    incoming_stock: Number,
    incoming_orders: Number,

    shipping_delay: Number,
    shipping_further_delays: Number,

    shipping_information: Number,
    customer_demand: Number,
    inventories: Number,
    send_messages: Number,

    end_round: Number,
    max_rounds: Number,

    created: {
        type: Date,
        default: Date.now
    },
    in_progress: Number,
    current_round: Number,

    // roles
    manufacturer: {
        type: Schema.ObjectId,
        ref: 'Business'
    },
    distributor: {
        type: Schema.ObjectId,
        ref: 'Business'
    },
    wholesaler: {
        type: Schema.ObjectId,
        ref: 'Business'
    },
    retailer: {
        type: Schema.ObjectId,
        ref: 'Business'
    }

});


module.exports = mongoose.model('Games', gameScehma);
