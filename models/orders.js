var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ordersScehma = mongoose.Schema({

    fromBusiness: {
        type: Schema.ObjectId,
        ref: 'business'
    },
    toBusiness: {
        type: Schema.ObjectId,
        ref: 'business'
    },
    // how many orders we're making
    orders: Number,
    // when we ordered
    starting_round: Number,
    // additional delay due to anomally
    additional_delay: Number    

});

module.exports = mongoose.model('Orders', ordersScehma);

