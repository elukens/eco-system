var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var messageScehma = mongoose.Schema({

    fromUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    toUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    subject: String,
    in_reference_to: {
        type: Schema.ObjectId,
        ref: 'Messages'
    },
    message: String,
    created: {
        type: Date,
        default: Date.now
    },
    read_status: Number,
    reply_status: Number,

    
    // include orders
    // include bills

});



module.exports = mongoose.model('Messages', messageScehma);
