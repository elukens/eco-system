var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var businessScehma = mongoose.Schema({

    name: String,

    // make this an array eventually
    players: {
        type: Schema.ObjectId,
        ref: 'User'
    },

    // stock on hand
    stock: Number,
    // back orders not filled
    backorders: Number,
    // total orders
    orders: Number,
    // we're done with our round
    complete: Number

});

module.exports = mongoose.model('Business', businessScehma);

